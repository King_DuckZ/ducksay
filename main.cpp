#include "main.hpp"
#include <cstring>
#include <cstdlib>
#include <sstream>
#include <cstdio>
#include <sys/stat.h>
#include <cctype>
#include "Sentences.hpp"
#include "Utilities.hpp"
#include <ctime>

namespace {
	const float BDayProb = 0.75f;

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool FileExists (const std::string& parPath) {
		struct stat sts;
		return static_cast<bool> ((stat(parPath.c_str(), &sts)) != -1);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	std::string ExecCmd (const std::string& parCommand) {
		std::FILE* pipe = popen(parCommand.c_str(), "r");
		if (not pipe)
			throw std::runtime_error("Can't open pipe");

		char buffer[128];
		std::string retVal;
		while(not std::feof(pipe)) {
			if (std::fgets(buffer, 128, pipe))
				retVal += buffer;
		}
		pclose(pipe);
		return retVal;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	std::set<std::string> GetAvailableCows() {
		const std::string cowPathString("Cow files in /");
		std::string currPath;
		const std::string listRaw(ExecCmd("cowsay -l"));
		std::set<std::string> retVal;

		size_t startIndex = 0;
		while (startIndex < listRaw.size()) {
			if (listRaw.compare(startIndex, std::min(listRaw.size() - startIndex, cowPathString.size()), cowPathString) == 0) {
				startIndex += cowPathString.size();
				currPath = "/";
				size_t index = 0;
				while (index + startIndex < listRaw.size()) {
					const char currChara = listRaw[index + startIndex];
					++index;
					if (currChara != ':')
						currPath += currChara;
					else
						break;
				}
				startIndex += index;
				currPath += "/";
			}
			else {
				std::string currFile(currPath);
				std::string currSubstr;
				size_t index = 1;
				while (startIndex + index < listRaw.size()) {
					currSubstr = listRaw.substr(startIndex, index);
					if (startIndex + index == listRaw.size() - 1 or listRaw[startIndex + index] == ' ' or listRaw[startIndex + index] == '\n') {
						currFile = currPath + currSubstr + ".cow";
						if (FileExists(currFile)) {
							retVal.insert(currSubstr);
							break;
						}
					}
					++index;
				}
				startIndex += index;
			}

			//Skip blanks
			while (startIndex < listRaw.size() and (listRaw[startIndex] == ' ' or listRaw[startIndex] == '\n')) ++startIndex;
		}
		return retVal;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void AppendParamsToString (int parArgsCount, char* parArgs[], std::ostream& parOut) {
		for (int z = 0; z < parArgsCount; ++z) {
			parOut << parArgs[z] << " ";
		}
	}

	///-------------------------------------------------------------------------
	///Date must be in YYYYMMDD format.
	///-------------------------------------------------------------------------
	unsigned int GetBDay (const char* parString) {
		const int l = std::strlen(parString);
		if (l != 8)
			return 0;
		for (int z = 0; z < 8; ++z)
			if (not std::isdigit(parString[z]))
				return 0;
		return static_cast<unsigned int>(std::atoi(parString));
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool HasUserDefinedFigure (int parCount, const char* const* parArgs) {
		for (int z = 0; z < parCount; ++z) {
			if (std::strcmp(parArgs[z], "-f") == 0 and z < parCount - 1)
				return true;
		}
		return false;
	}
} //unnamed namespace

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
int main (int argc, char* argv[]) {
	const unsigned int bday = (argc >= 3 ? GetBDay(argv[1]) : 0);
	const int cowTypeIndex = (bday ? 2 : 1);
	const bool automatic = (argc >= 2 and std::strncmp(argv[cowTypeIndex], "auto", 4) == 0);
	const int cowTypeStrOffset = (automatic ? 4 : 0);
	const bool think = (argc >= 2 and std::strcmp(argv[cowTypeIndex] + cowTypeStrOffset, "think") == 0);
	const bool say = (argc >= 2 and std::strcmp(argv[cowTypeIndex] + cowTypeStrOffset, "say") == 0);
	const bool stats = (argc >= 2 and not automatic and std::strcmp(argv[cowTypeIndex], "stats") == 0);
	if (not (think xor say) and not stats) {
		std::cerr << "Usage: " << argv[0] << " [date] <stats|[auto]say|[auto]think> [parameters to cowsay [...]]\n";
		std::cerr << "Please email bugs and requests to king_duckz@gmx.com" << std::endl;
		return 2;
	}

	if (stats) {
		PrintStats();
		return 0;
	}

	std::srand(std::time(NULL));

	const std::set<std::string> availableCows(GetAvailableCows());
	std::ostringstream command;

	command << "cow" << (argv[cowTypeIndex] + cowTypeStrOffset) << " ";
	if (not HasUserDefinedFigure(argc, argv)) {
		const std::string defCow("duck");
		if (availableCows.find(defCow) != availableCows.end())
			command << "-f \"" << defCow << "\" ";
	}
	AppendParamsToString(argc - 1 - cowTypeIndex, argv + 1 + cowTypeIndex, command);

	if (automatic) {
		SentenceKind kind;
		if (bday != 0 and IsBDayToday(bday) and static_cast<float>(std::rand()) / RAND_MAX <= BDayProb)
			kind = SentenceKind_Anniversary;
		else
			kind = SentenceKind_Regular;

		const Sentence& currSentence = GetRandomSentence(kind);
		if (currSentence.eyes)
			command << "-e '" << currSentence.eyes << "' ";
		if (currSentence.tongue)
			command << "-T '" << currSentence.tongue << "' ";
		bool echoMessage = false;
		if (currSentence.options) {
			command << currSentence.options << " ";
			if (std::strstr(currSentence.options, "-n")) {
				//man page for cowsay states that if -n is used, nothing should
				//be left on the command line after switches are parsed.
				echoMessage = true;
			}
		}

		if (echoMessage) {
			//std::ostringstream oldCommand;
			//oldCommand.swap(command);
			const std::string oldCommand(command.str());
			command.str(std::string());
			command << "echo $'" << ReplaceTokensInString(currSentence.sentence) << "'|" << oldCommand;
		}
		else {
			command << "$'" << ReplaceTokensInString(currSentence.sentence) << "'";
		}
	}
	//std::cout << command.str() << "\n";
	const int retVal = system(command.str().c_str());
	return retVal;
}
