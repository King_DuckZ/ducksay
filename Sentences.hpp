#ifndef idB3E3351829824F55BA4B6C6E87B455F5
#define idB3E3351829824F55BA4B6C6E87B455F5

struct Sentence {
	const char* sentence;
	const char* tongue;
	const char* eyes;
	const char* options;
};

enum SentenceKind {
	SentenceKind_Regular,
	SentenceKind_Anniversary
};

const Sentence& GetRandomSentence ( SentenceKind parKind );
void PrintStats ( void );
std::string ReplaceTokensInString ( std::string parText );

#endif
