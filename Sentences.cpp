#include "main.hpp"
#include "Sentences.hpp"
#include <cstdlib>
#include <cstring>
#include "Utilities.hpp"
#include <cctype>
#include <sys/ioctl.h>
#include <sstream>

#ifndef lengthof
#define UNDEF_lengthof
#define lengthof(a) (sizeof(a) / sizeof(a[0]))
#endif

//#define LASTSENTENCEONLY

namespace {
	template <typename T>
	class StringFetcher;

	template <int N>
	class StringFetcher<char[N]> {
	public:
		typedef std::string ConstStringType;
		static ConstStringType FetchFrom ( const char (&parFrom)[N] ) { return ConstStringType(parFrom); }
	};
	template <>
	class StringFetcher<std::string> {
	public:
		typedef const std::string& ConstStringType;
		static ConstStringType FetchFrom ( ConstStringType parFrom ) { return parFrom; }
	};
	template <>
	class StringFetcher<std::string (*)()> {
	public:
		typedef std::string ConstStringType;
		static ConstStringType FetchFrom ( std::string (*parFunction)() ) { return (*parFunction)(); }
	};

	const Sentence Sentences[] = {
		{"Will you stop following me already?", NULL, "><", NULL},
		{"I'm not sure I've told you yet, but I don't believe you're King_DuckZ for real.", NULL, NULL, NULL},
		{"Swim, put head underwater, beak my back... So many things to do today, we'd better get down to work...", NULL, NULL, "-p"},
		{"Did you know that (psst psst psst) and then (psst pssst)? But shh! That's a secret!", NULL, NULL, "-y"},
		{"From the depths I come!", NULL, NULL, "-b"},
		{"There is no such thing as innocence, only degrees of guilt.", NULL, NULL, "-b"},
		{"In the pipe, five by five!", NULL, NULL, "-b"},
		{"Battlecruiser operational!", NULL, NULL, "-b"},
		{"I wondered why the baseball was getting bigger. Then it hit me.", NULL, NULL, "-d"},
		{"Police were called to a day care where a 3-yr-old was resisting a rest.", NULL, NULL, "-y"},
		{"Did you hear about the guy whose whole left side was cut off? He's all right now.", NULL, NULL, "-p"},
		{"The roundest knight at King Arthur's round table was Sir Cumference.", NULL, NULL, NULL},
		{"The butcher backed up into the meat grinder & got a little behind in his work.", NULL, NULL, NULL},
		{"To write with a broken pencil is pointless.", NULL, NULL, "-y"},
		{"When fish are in schools, they sometimes take debate.", NULL, NULL, NULL},
		{"A thief who stole a calendar got 12 months.", NULL, NULL, NULL},
		{"A thief fell into wet cement & became a hardened criminal.", NULL, NULL, NULL},
		{"The batteries were given out free of charge.", NULL, NULL, NULL},
		{"A dentist & a manicurist fought tooth and nail.", NULL, NULL, NULL},
		{"If you don't pay your exorcist you can get repossessed.", NULL, NULL, NULL},
		{"The guy who had fallen onto an upholstery machine was fully recovered.", NULL, NULL, NULL},
		{"You are stuck with your debt if you can't budge it.", NULL, NULL, NULL},
		{"A calendar's days are numbered.", NULL, NULL, NULL},
		{"A boiled egg is hard to beat.", NULL, NULL, "-s"},
		{"Those who get too big for their britches will be exposed in the end.", NULL, NULL, NULL},
		{"When she saw her first strands of gray hair, she thought she'd dye.", NULL, NULL, NULL},
		{"When Life Gives You Questions, DuckDuckGo has Answers.", NULL, NULL, NULL},
		{"If at first you don't succeed, call it version 1.0.", NULL, NULL, NULL},
		{"There are 10 types of people in the world: those who understand binary, and those who don't. Sorry but I couldn't resist, I just had to say that one.", NULL, NULL, "-p"},
		{"There are 10 types of people in the world: those who understand binary, and those who don't.", NULL, NULL, NULL},
		{"I'm not anti-social; I'm just not user friendly.", NULL, NULL, "-T"},
		{"The glass is neither half-full nor half-empty: it's twice as big as it needs to be.", NULL, NULL, NULL},
		{"I would love to change the world, but they won't give me the source code.", NULL, "uu", NULL},
		{"Passwords are like underwear: you shouldn't leave them out where people can see them; you should change them regularly; you shouldn't loan them out to strangers.", NULL, NULL, NULL},
		{"A Life? Cool! Where can I download one of those?", NULL, NULL, "-w"},
		{"Windows has detected you do not have a keyboard. Press 'F9' to continue.", NULL, NULL, "-b"},
		{"In a world without fences and walls, who needs Gates and Windows?", NULL, NULL, NULL},
		{"People say that if you play Microsoft CDs backwards you hear satanic things, but that's nothing, because if you play them forwards, they install Windows.", NULL, NULL, NULL},
		{"You have just received the Amish Computer Virus. Since the Amish don't have computers, it is based on the honor system. So please delete all the files from your computer. Thank you for you cooperation.", NULL, NULL, NULL},
		{"Never make fun of the geeks, one day they will be your boss.", NULL, "o~", NULL},
		{"My software never has bugs. It just develops random features.", NULL, NULL, NULL},
		{"The speed of sound is defined by the distance from door to computer divided by the time interval needed to close the media player and pull up your pants when your mom shouts \"OH MY GOD WHAT ARE YOU DOING!!!\"", NULL, NULL, NULL},
		{"9x-7i > 3(3x-7u) = i <3 u :)", NULL, NULL, NULL},
		{"Girls are like internet domain names, the ones I like are already taken.", NULL, NULL, NULL},
		{"Please enter any 11-digit prime number to continue...", NULL, NULL, "-b"},
		{"Software is like sex: It's better when it's free.", NULL, NULL, NULL},
		{"Someone once said a million monkeys using a million keyboards could reproduce the complete works of William Shakespeare. Thanks to MySpace, we now know that to be entirely false.", NULL, NULL, NULL},
		{"That's a PEBKAC problem (Problem Exists Between Keyboard And Chair).", NULL, NULL, NULL},
		{"Alcohol & calculus don't mix. Never drink & derive.", NULL, NULL, "-s"},
		{"A thousand words are worth a picture, and they load a heck of a lot faster.", NULL, NULL, NULL},
		{"You know it's love when you memorize her IP number to skip DNS overhead.", NULL, NULL, NULL},
		{"Ethernet (n): something used to catch the etherbunny", NULL, NULL, NULL},
		{"When life gives you a keyboard,\\nmake a bunny rabbit.\\n\\n (\\\\ (\\\\\n(='.')\\n((^)(^)", NULL, NULL, "-n"},
		{"I wish I could find someone like Google. Someone to finish my thoughts, correct my mistakes without making fun of me, help me with anything whenever I need it. Someone who would be always there for me no matter what. Google, you have everything I need in a guy. You're my prince charming!", NULL, NULL, NULL},
		{"Alert! User Error. Please replace user and press any key to continue.", NULL, NULL, NULL},
		{"The code that is the hardest to debug is the code that you know cannot possibly be wrong.", NULL, NULL, NULL},
		{"Video games are bad for you? That's what they said about Rock-n-Roll.", NULL, NULL, NULL},
		{"Without geometry, life is pointless.", NULL, NULL, NULL},
		{"I spent a minute looking at my own code by accident. I was thinking \"What the hell is this guy doing?\"", NULL, NULL, NULL},
		{"Unix, DOS and Windows... the good, the bad and the ugly.", NULL, NULL, NULL},
		{"I AM NOT YOUR F1 BUTTON!", NULL, "><", NULL},
		{"It is fruitless to become lachrymose because of scattered lacteal fluid!!! (don't cry over spilled milk!)", NULL, "TT", NULL},
		{"Beware of computer programmers that carry screwdrivers.", NULL, NULL, NULL},
		{"I don't care if the software I run is unstable crap, as long as it is the LATEST unstable crap.", NULL, NULL, NULL},
		{"You're just jealous because the voices only talk to me.", NULL, NULL, NULL},
		{"I'm not nerdy. I'm intellectually endowed.", NULL, NULL, NULL},
		{"Dear Jocks, you say you have had 10 times more girlfriends then me but 10x0=0. Sincerly the nerds", NULL, NULL, NULL},
		{"Hacking is like sex. You get in, you get out, and hope that you didn't leave something that can be traced back to you.", NULL, NULL, NULL},
		{"The hard drive on your computer will only crash when it contains vital information that has not been backed up.", NULL, NULL, "-t"},
		{"Trolling is a art.", NULL, NULL, NULL},
		{"Just in case you were wondering, it's the %DATE%. It won't change until tomorrow, you know.", NULL, NULL, "-y"},
		{"I just can't seem to hack your heart and spam you with my words.", NULL, NULL, NULL},
		{"The truth is out there... do you have the URL?", NULL, NULL, NULL},
		{"Who ever called geeks anti-social, we have a social life!!! Haven't you ever played World of Warcraft?", NULL, NULL, NULL},
		{"you know the velocity of light... But what is the velocity of darkness...??", NULL, NULL, NULL},
		{"Everyone starts as a noob. Not everyone ends like a pro...", NULL, NULL, NULL},
		{"The problem with \"logic\" is that it\\nmakes things out to be nothing but simple\\ndualities. Proponents of logic want us to\\nbelieve that everything is true or false,\\nblack or white, yes or no.\\nThankfully, there are some among us, who\\nare not afraid to stand up to these logic\\nadvocates and shout \"no, I will not\\nsuccumb to your false dichotomies!\" Today,\\nI think we all should salute those few brave\\npeople...\\n\\nenum Bool\\n{\\n\tTrue,\\n\tFalse,\\n\tFileNotFound\\n};", NULL, NULL, "-n"},
		{"plz email me teh codez!!!!1", NULL, "TT", NULL},
		{"Did you ever hear of someone called $USER? Or was it... oh, never mind.", NULL, NULL, NULL},
		{"Honestly, I think $USER is a very nice person... if only... oh, never mind, let's just get back to swimming.", NULL, NULL, "-y"},
		{"Whenever I look at the calendar today it says %DATE%. Did it freez... uh... stop working?", NULL, NULL, "-p"},
		{"I used to be a great swimmer. But then I took an arrow to the knee!", NULL, NULL, "-p"},
		{"I used to be an excellent detective. But then I took an arrow to the knee!", NULL, NULL, "-p"},
		{"I heard $USER used to be a good programmer. But then he took an arrow to the knee!", NULL, NULL, "-p"},
		{"$USER used to be a fleet admiral. But then he took an arrow to the knee!", NULL, NULL, "-p"},
		{"I think I'm going to write my own mmorpg. I only need to find a programmer, a musician and a graphist!", NULL, NULL, "-y"},
		{"Just move to the Internet, its great here. We get to live inside where the weather is always awesome.", NULL, NULL, NULL},
		{"Nerd. Geek. Used to be if you self-identified that way, you'd get thrown into a locker and never have sex. Or worse, whatever that is. But to me and more and more people I know, being a nerd or a geek means having passion, power, intelligence. Being a nerd just means there is something in the world that you care deeply about--be it twelve-sided dice, a favorite sports team, your new laptop or Knight Rider.", NULL, NULL, NULL},
		{"Like the famous mad philosopher said, when you stare into the void, the void stares also; but if you cast into the void, you get a type conversion error (which just goes to show Nietzsche wasn't a C++ programmer).", NULL, NULL, NULL},
		{"There is no place like 127.0.0.1. Why, maybe $HOSTNAME...", NULL, NULL, NULL},
		{"Programming today is a race between software engineers striving to build bigger and better idiot-proof programs, and the Universe trying to produce bigger and better idiots. So far, the Universe is winning.", NULL, "uu", NULL},
		{"Squash one bug, you’ll see ten new bugs popping.", NULL, NULL, NULL},
		{"Everytime $USER touches his code, he gives birth to ten new bugs.", NULL, NULL, NULL},
		{"sudo make me a sandwich", NULL, NULL, NULL},
		{"Ok everyone stand back: I know regular expressions.", NULL, NULL, NULL},
		{"Why doesn’t DOS ever say \"EXCELLENT command or filename!\"", NULL, NULL, NULL},
		{"A typical Yahoo! inbox: Inbox(0), Junk(9855210)", NULL, NULL, NULL},
		{"C++ is a write-only language: one can write programs in C++, but I can't read any of them.", NULL, NULL, NULL},
		{"You must think that swimming in a %COLUMNS%x%LINES% pool is funny... It would be, my dear $USER, if only there wasn't a creep following me like a shadow.", NULL, NULL, NULL}
	};

	const Sentence BDaySentences[] = {
		{"You know, it's %DATE% today. Wasn't there something I had to remember...?", NULL, NULL, NULL},
		{"They say the %DATE% is a funny day... I wonder why?", NULL, NULL, NULL},
		{"Congrats for your level up! It doesn't show much tho...", NULL, NULL, NULL},
		{"What are you doing here, don't you see the party's started without you?", NULL, NULL, "-s"},
		{"Happy %DATE% my dear follower!", NULL, NULL, "-b"},
		{"...the post on bitmit is done, on ebay too... Uh? How should I know where you presents are?", NULL, NULL, "-g"},
		{"At last you leveled up! I almost though you didn't want to!", NULL, NULL, NULL},
		{"It's the %DATE%, why do you ask? Is there something I should know?", NULL, NULL, NULL},
		{"Right, right, it's the %DATE%, fine. Now git!", NULL, NULL, "-t"},
		{"...the cake, the balloons... candles are there, party hats... Uh? Wait, you're not supposed to hear this!", NULL, NULL, "-o"},
		{"Isn't $USER's birthday on the %DATE%? Uh wait, that's today!", NULL, NULL, "-w"},
		{"I did buy a present for you, $USER... But a dog ate it. By the way, isn't my presence a present in itself?", NULL, NULL, NULL},
		{"There's something involving $USER and the %DATE% that I know I should remember... Do you happen to know what it was?", NULL, NULL, "-t"}
	};

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	template <typename T>
	void ReplaceAll (std::string& parText, const char* parSearch, const T& parReplace) {
		size_t replFrom;
		if ((replFrom = parText.find(parSearch)) != std::string::npos) {
			typename StringFetcher<T>::ConstStringType replacement(StringFetcher<T>::FetchFrom(parReplace));
			const int replacedStringSize = std::strlen(parSearch);
			do {
				parText.replace(replFrom, replacedStringSize, replacement);
			} while ((replFrom = parText.find(parSearch, replFrom + replacement.size())) != std::string::npos);
		}
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	std::string GetEnvVarReplacement (const std::string& parName) {
		const char* const variable = std::getenv(parName.c_str());
		if (variable)
			return std::string(variable);
		else
			return std::string();
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	std::string GetTerminalColumns() {
		struct winsize w;
		ioctl(0, TIOCGWINSZ, &w);
		std::ostringstream oss;
		oss << w.ws_col;
		return oss.str();
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	std::string GetTerminalRows() {
		struct winsize w;
		ioctl(0, TIOCGWINSZ, &w);
		std::ostringstream oss;
		oss << w.ws_row;
		return oss.str();
	}
} //unnamed namespace

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
const Sentence& GetRandomSentence (SentenceKind parKind) {
	const Sentence* sentenceList;
	int sentenceCount;

	static const Sentence dummySentence = {"Dummy sentence - something's wrong, sorry", NULL, NULL, "-d"};

	switch (parKind) {
	case SentenceKind_Regular:
		sentenceList = Sentences;
		sentenceCount = lengthof(Sentences);
		break;

	case SentenceKind_Anniversary:
		sentenceList = BDaySentences;
		sentenceCount = lengthof(BDaySentences);
		break;

	default:
		return dummySentence;
	}

#if defined(LASTSENTENCEONLY)
	const unsigned int sentenceNum = sentenceCount - 1;
#else
	const unsigned int sentenceNum = std::rand() % sentenceCount;
#endif
	return sentenceList[sentenceNum];
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
void PrintStats() {
	std::cout << "Regular list: " << lengthof(Sentences) << "\n";
	std::cout << "Birthday list: " << lengthof(BDaySentences) << "\n";
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
std::string ReplaceTokensInString (std::string parText) {
	ReplaceAll(parText, "%DATE%", &GetDateAsString);
	ReplaceAll(parText, "'", "\\'");
	ReplaceAll(parText, "%LINES%", &GetTerminalRows);
	ReplaceAll(parText, "%COLUMNS%", &GetTerminalColumns);

	{
		size_t index = 0;
		while (std::string::npos != (index = parText.find('$', index))) {
			if (index == 0 or parText[index - 1] != '\\') {
				size_t varNameEnd = index + 1;
				if (index < parText.size() - 1 and parText[index + 1] == '{') {
					varNameEnd = parText.find('}', index + 1);
					if (varNameEnd == std::string::npos)
						varNameEnd = parText.size();
					if (varNameEnd > index + 2) {
						const std::string strVarName(parText.substr(index, varNameEnd - index + 1));
						const std::string envRepl(GetEnvVarReplacement(strVarName.substr(2, strVarName.size() - 3)));
						parText.replace(index, strVarName.size(), envRepl);
						varNameEnd = index + envRepl.size() - 1;
					}
					index = varNameEnd + 1;
				}
				else {
					while (varNameEnd < parText.size() and std::isalnum(parText[varNameEnd])) ++varNameEnd;
					if (varNameEnd > index + 1) {
						const std::string strVarName(parText.substr(index, varNameEnd - index));
						const std::string envRepl(GetEnvVarReplacement(strVarName.substr(1, std::string::npos)));
						parText.replace(index, strVarName.size(), envRepl);
						varNameEnd = index + envRepl.size();
					}
					index = varNameEnd;
				}
			}
			else if (index > 0 and parText[index - 1] == '\\') {
				parText.erase(index - 1, 1);
			}
			else {
				++index;
			}
		}
	}
	return parText;
}

#ifdef UNDEF_lengthof
#undef UNDEF_lengthof
#undef lengthof
#endif
#if defined(LASTSENTENCEONLY)
#undef LASTSENTENCEONLY
#endif
