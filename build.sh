#!/usr/bin/env bash
buildtype=release
if [[ $# == 1 ]]; then
	buildtype=$1
fi

if [[ $buildtype == "debug" ]]; then
	g++ -Wall -g -O0 -o ducksaywrapperdebug main.cpp Sentences.cpp Utilities.cpp
elif [[ $buildtype == "release" ]]; then
	g++ -Wall -O3 -o ducksaywrapper -fomit-frame-pointer -DNODEBUG main.cpp Sentences.cpp Utilities.cpp
else
	echo "Invalid configuration: $buildtype"
	exit 2
fi
