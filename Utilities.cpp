#include "main.hpp"
#include <cstdlib>
#include <ctime>
#include <sstream>

namespace {
	const char* Months[] = {
		"January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"
	};
} //unnamed namespace

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
std::string GetDateAsString() {
	std::time_t rawTime;
	std::time(&rawTime);
	struct std::tm* const timeInfo = std::localtime(&rawTime);
	std::ostringstream ossDate;
	ossDate << timeInfo->tm_mday;
	if (timeInfo->tm_mday != 11 and timeInfo->tm_mday % 10 == 1)
		ossDate << "st";
	else if (timeInfo->tm_mday != 12 and timeInfo->tm_mday % 10 == 2)
		ossDate << "nd";
	else if (timeInfo->tm_mday != 13 and timeInfo->tm_mday % 10 == 3)
		ossDate << "rd";
	else
		ossDate << "th";
	ossDate << " of " << Months[timeInfo->tm_mon];
	return ossDate.str();
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
unsigned int GetTodayDate() {
	std::time_t rawTime;
	std::time(&rawTime);
	struct std::tm* const timeInfo = std::localtime(&rawTime);
	const unsigned int retVal = (timeInfo->tm_year + 1900) * 10000 +
		(timeInfo->tm_mon + 1) * 100 +
		timeInfo->tm_mday;
	return retVal;
}

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
bool IsBDayToday (unsigned int parBDay) {
	const unsigned int today = GetTodayDate();
	const unsigned int anniv = (parBDay	% 10000) + today / 10000 * 10000;
	return static_cast<bool>(anniv == today);
}
